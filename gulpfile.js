var gulp = require('gulp');
var sass = require('gulp-sass');

var paths = {
    scripts: './src/main/resources/public/js/**/*.js',
    images: './src/main/resources/public/img/**/*',
    sass: './src/main/resources/public/css/scss/*.scss'
};
var buildpaths = {
    css: './src/main/resources/public/css',
    js: './src/main/resources/public/js',
    libs: './src/main/resources/public/libs'
}

var bowerComponents = {
    scripts: [
        './bower_components/angular/angular.min.js',
        './bower_components/angular/angular.min.js.map',
        './bower_components/angular-route/angular-route.min.js',
        './bower_components/angular-route/angular-route.min.js.map',
        './bower_components/foundation/js/foundation.min.js',
        './bower_components/foundation/js/vendor/modernizr.js',
        './bower_components/jquery/dist/jquery.min.js',
        './bower_components/jquery/dist/jquery.min.map'],
    css: [
        './bower_components/foundation/css/*.css',
        './bower_components/bootstrap/dist/css/bootstrap.min.css',
        ]
}


gulp.task('default', ['watch','sass']);
gulp.task('compile', ['sass', 'copyBowerComponents']);
gulp.task('build', ['sass','copyBowerComponents']);

gulp.task('sass', function () {
    gulp.src(paths.sass)
        .pipe(sass())
        .pipe(gulp.dest(buildpaths.css));
});

gulp.task('scripts', function(){

});

gulp.task('copyBowerComponents', function(){
    gulp.src(bowerComponents.scripts,{base: './bower_components'})
        .pipe(gulp.dest(buildpaths.libs))
    gulp.src(bowerComponents.css,{base: './bower_components'})
        .pipe(gulp.dest(buildpaths.libs))
})

// Rerun the task when a file changes
gulp.task('watch', function() {
    gulp.watch(paths.scripts, ['scripts']);
    gulp.watch(paths.sass, ['sass']);
});