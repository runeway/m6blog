//var xeon = require('/lib/xeon');
/*var menuService = require('menuService');
var contentService = require('contentService');

var thymeleaf = require('/lib/view/thymeleaf');

function handleGet(req) {
    var params = defaultParams(req);
    var view = resolve('../../view/page42.html');
    var body = thymeleaf.render(view, params);

    return {
        body: body,
        contentType: 'text/html'
    };
}


}*/

var thymeleaf = require('view/thymeleaf');

exports.get = function(req) {
    var params = defaultParams(req);
    var view = resolve('../../view/page42.html');
    var body = thymeleaf.render(view, params);
    return {
        body: body,
        contentType: 'text/html'
    };
};


function defaultParams(req){
    var site = execute('portal.getSite');
    var content = execute('portal.getContent');
    var moduleConfig = site.moduleConfigs['com.enonic.wem.modules.m6blog'];

    return {
        name: "Rune",
        context: req,
        site: site,
        content: content,
        mainRegion: content.page.regions["main"],
        moduleConfig: moduleConfig
        //mainRegion: req.content.page.getRegion("main"),
        //menuItems: menuService.getSiteMenuAsList(req.site),
        //banner: false,
        //editable: editMode,
        //moduleConfig: moduleConfig,
        //logoUrl: this.getLogoUrl(req, moduleConfig)
    }
}