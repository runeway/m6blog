angular.module('BlogApp.controllers', [])

    .controller('linkController', function($scope) {
        $scope.links = [
            {
                url: "#blogroll",
                description: "Blogroll"
            },
            {
                url: "#inspiration",
                description: "Inspiration"
            },
            {
                url: "#links",
                description: "Links"
            }
        ];
    })

    .controller('categoriesController', function($scope) {
        $scope.categories = [
            {
                name: "Poetry"
            },
            {
                name: "Technology"
            },
            {
                name: "Fish"
            },
            {
                name: "Afrika"
            }
        ];
    });

