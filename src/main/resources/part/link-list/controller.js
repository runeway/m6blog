var thymeleaf = require('view/thymeleaf');

exports.get = function(portal) {
    var component = execute('portal.getComponent');
    var links = component.config["link"] || [];

    var params = {
        component: component,
        links: links
    };

    var view = resolve('./link-list.html');
    var body = thymeleaf.render(view, params);

    return {
        body: body,
        contentType: 'text/html'
    };
};