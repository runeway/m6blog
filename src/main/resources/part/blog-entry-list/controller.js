var thymeleaf = require('view/thymeleaf');

exports.get = function(portal) {
    var component = execute('portal.getComponent');
    var blogentries = component.config["blogentry"] || [];

    var params = {
        component: component,
        blogentries: blogentries
    };

    var view = resolve('./blog-entry-list.html');
    var body = thymeleaf.render(view, params);

    return {
        body: body,
        contentType: 'text/html'
    };
};