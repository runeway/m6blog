var thymeleaf = require('view/thymeleaf');

exports.get = function(req) {

    var component = execute('portal.getComponent');

    var params = {
        component: component,
        leftRegion: component.regions["left"],
        rightRegion: component.regions["right"]
    }

    var view = resolve('./layout-70-30.html');
    var body = thymeleaf.render(view, params);

    return {
        body: body,
        contentType: 'text/html'
    }

};